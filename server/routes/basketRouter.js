const Router = require("express");
const router = new Router();
const basketController = require("../controllers/basketController");
// const checkRole = require("../middleware/checkRoleMiddleware");

router.post("/", basketController.addItem);
router.get("/:userId", basketController.getUserBasket);
router.get("/", basketController.getAllBasketDevices);
router.put("/", basketController.removeItem);
router.patch("/", basketController.removeSingleItem);

module.exports = router;