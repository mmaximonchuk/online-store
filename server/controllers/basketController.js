const { Basket, BasketDevice, Device } = require("../models/models");
const ApiError = require("../error/ApiError");
class BasketController {
  async addItem(req, res, next) {
    const { deviceId, userId } = req.body;
    if(!userId) next(ApiError.badRequest({ message: "You should authorize first to add items!" }));

    const isUserBasketCreated = await Basket.findOne({ where: { userId } });

    if (isUserBasketCreated === null) {
      const basket = await Basket.create({ userId });

      await BasketDevice.create({
        deviceId,
        basketId: basket.id,
      });
      const updatedUserBasket = await Basket.findOne({
        where: { userId },
        include: [{ model: BasketDevice }],
      });

      return res.json(updatedUserBasket);
    }

    await BasketDevice.create({
      deviceId,
      basketId: isUserBasketCreated.id,
    });

    const updatedUserBasket = await Basket.findOne({
      where: { userId },
      include: [{ model: BasketDevice }],
    });

    return res.json(updatedUserBasket);
  }

  async getUserBasket(req, res) {
    const { userId } = req.params;
    const basket = await Basket.findOne({
      where: { userId },
      include: [{ model: BasketDevice }],
    });

    return res.json(basket);
  }

  async getAllBasketDevices(req, res) {
    const { userId } = req.query;
    const {basket_devices} = await Basket.findOne({
      where: { userId },
      include: [{ model: BasketDevice }],
    });
    const devicesIds = basket_devices.map(device => device.deviceId)

    const basketDevices = await Device.findAll({
      where: {
        id: devicesIds
        // {
          // [Op.and]: devicesIds
        // }
      }
    })

    return res.json(basketDevices);
  }
  async removeItem(req, res) {
    const { userId, deviceId } = req.body;

    const userBasket = await Basket.findOne({
      where: { userId },
    });

    await BasketDevice.destroy({
      where: { basketId: userBasket.id, deviceId },
    });

    const updatedUserBasket = await Basket.findOne({
      where: { userId },
      include: [{ model: BasketDevice }],
    });

    return res.json(updatedUserBasket);
  }

  async removeSingleItem(req, res) {
    const { userId, itemId } = req.body;

    const userBasket = await Basket.findOne({
      where: { userId },
    });

    await BasketDevice.destroy({
      where: { basketId: userBasket.id, id: itemId },
    });

    const updatedUserBasket = await Basket.findOne({
      where: { userId },
      include: [{ model: BasketDevice }],
    });

    return res.json(updatedUserBasket);
  }
}

module.exports = new BasketController();
