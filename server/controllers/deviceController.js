const uuid = require("uuid");
const sequelize = require("../db");
const path = require("path");
const { Device, DeviceInfo, Rating } = require("../models/models");
const ApiError = require("../error/ApiError");
class deviceController {
  async create(req, res, next) {
    try {
      let { name, price, brandId, typeId, info } = req.body;
      const { img } = req.files;
      let fileName = uuid.v4() + ".png";
      img.mv(path.resolve(__dirname, "..", "static", fileName));
      const device = await Device.create({
        name,
        price,
        brandId,
        typeId,
        img: fileName,
      });

      if (info.length) {
        info = JSON.parse(info);
        info.forEach((i) =>
          DeviceInfo.create({
            title: i.title,
            description: i.description,
            deviceId: device.id,
          })
        );
      }

      return res.json(device);
    } catch (e) {
      next(ApiError.badRequest(e.message));
    }
  }
  async delete(req, res, next) {
    try {
      let { id } = req.params;

      const device = await Device.destroy({where: { id  }})

      return res.json(device);
    } catch (e) {
      next(ApiError.badRequest(e.message));
    }
  }
  async getAll(req, res) {
    let { brandId, typeId, limit = 9, page = 1 } = req.query; // ?limit=5&page=4&brandId=Apple
    let offset = page * limit - limit;
    let devices = [];
    let devicesToReturn = { rows: [], count: 0 };
    let devicesLenght = 0;

    if (!brandId && !typeId) {
      // devices = await Device.findAndCountAll({
      //   limit,
      //   offset,
      //   include: [{ model: Rating, required: false }],
      // });

      devices = await Device.findAll({
        limit,
        offset,
        include: [{ model: Rating }],
      });
      devicesLenght = await Device.findAll();

      devicesToReturn = {
        rows: devices,
        count: devicesLenght.length,
      };

      // console.log("devices: ", devices);
      // devices2 = await Device.findAll();
      // console.log('devices2: ', devices2);

      // devicesToReturn = {
      //   rows: [...devices.rows],
      //   count: devices2.length,
      // };
    }
    if (brandId && !typeId) {
      devices = await Device.findAll({
        where: { brandId },
        limit,
        offset,
        include: [{ model: Rating }],
      });
      devicesLenght = await Device.findAll({
        where: { brandId },
      });

      devicesToReturn = {
        rows: devices,
        count: devicesLenght.length,
      };
    }
    if (!brandId && typeId) {
      devices = await Device.findAll({
        where: { typeId },
        limit,
        offset,
        include: [{ model: Rating }],
      });
      devicesLenght = await Device.findAll({
        where: { typeId },
      });

      devicesToReturn = {
        rows: devices,
        count: devicesLenght.length,
      };
    }
    if (brandId && typeId) {
      devices = await Device.findAll({
        where: { brandId, typeId },
        limit,
        offset,
        include: [{ model: Rating }],
      });
      devicesLenght = await Device.findAll({ where: { brandId, typeId } });

      devicesToReturn = {
        rows: devices,
        count: devicesLenght.length,
      };
    }

    if (!devices.length) {
      return res.json({ message: "No devices found" });
    }

    return res.json(devicesToReturn);
  }

  async getOne(req, res) {
    const { id } = req.params;
    const device = await Device.findOne({
      where: { id },
      include: [{ model: DeviceInfo, as: "info" }, { model: Rating }],
    });
    return res.json(device);
  }
  async setDeviceRating(req, res, next) {
    const { deviceId, userId, rating } = req.body;
    if (rating < 0 || rating > 5) {
      next(ApiError.badRequest("Rating must be between 0 and 5"));
    }

    // const shouldRatingSetAvarage = await Rating.findAll({
    //   where: { deviceId },
    // });

    const shouldRatingUpdate = await Rating.findOne({
      where: { userId, deviceId },
    });

    if (shouldRatingUpdate) {
      const deviceRating = await Rating.update(
        {
          rate: rating,
        },
        { where: { userId, deviceId } }
      );
      // if (shouldRatingSetAvarage?.length) {
      //   const sumOfRating = countAndSetAverageRating.reduce((acc, curr) => {
      //     return acc.rate + curr.rate;
      //   }, 0);
      //   const averageRating = sumOfRating / countAndSetAverageRating.length;

      //   return res.json([{...deviceRating, rate: averageRating}]);
      // }
      return res.json(deviceRating);
    }

    const deviceRating = await Rating.create({
      deviceId,
      userId,
      rate: rating,
    });

    return res.json(deviceRating);
  }
}

module.exports = new deviceController();
