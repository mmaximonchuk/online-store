import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  basketData: [],
  basketItemsArray: [],
  fullPrice: 0,
};

// function countFullPrice() {
//   state.fullPrice = state.basketItemsArray.reduce((acc, item) => {
//     return acc = item.price * item.count
//   }, 0)
// }

export const basketSlice = createSlice({
  name: "basket",
  initialState,
  reducers: {
    removeBasketItem: (state, action) => {
      let count = state.basketData.basket_devices.reduce(
        (acc2, curr2) => (curr2.deviceId === action.payload ? (acc2 += 1) : acc2),
        0
      );
      if(count > 1) {
        state.basketItemsArray = state.basketItemsArray.map(item => item.id === action.payload ? {...item, count: item.count - 1} : item)
      } else {
        state.basketItemsArray = state.basketItemsArray.filter((device) => device.id !== action.payload);
      }
      state.fullPrice = state.basketItemsArray.reduce((acc, item) => {
        return acc += item.price * item.count
      }, 0)
    },
    addBasketItem: (state, action) => {
      // const prevData = ;
      // if(!state.basketItemsArray.some(i => i.id === action.payload.id)) {
      state.basketItemsArray = [...state.basketItemsArray, action.payload];
      // }
      // state.basketItemsArray = new Set(state.basketItemsArray);
      state.fullPrice = state.basketItemsArray.reduce((acc, item) => {
        return acc += item.price * item.count
      }, 0)
    },
    setBasketDevices: (state, action) => {
      // state.basketItemsArray = action.payload.reduce((acc1, curr1, index) => {
      //   let count = state.basketData.basket_devices.reduce(
      //     (acc2, curr2) => (curr2.deviceId === curr1.id ? (acc2 += 1) : acc2),
      //     0
      //   );
      //   debugger;
        
      //   return acc1[0] = {...curr1, count}

      // }, []);
      state.basketItemsArray = action.payload.map((item, index) => {
        let count = state.basketData.basket_devices.reduce(
          (acc2, curr2) => (curr2.deviceId === item.id ? (acc2 += 1) : acc2),
          0
        );
        return {...item, count}
      })
      state.fullPrice = state.basketItemsArray.reduce((acc, curr) => {
        return acc += curr.price * curr.count
      }, 0)
      // state.basketItemsArray = action.payload;
    },

    setBasketData: (state, action) => {
      state.basketData = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setBasketData,
  setBasketDevices,
  addBasketItem,
  removeBasketItem,
} = basketSlice.actions;
export const selectBasketData = (state) => state.basket.basketData;
export const selectBasketItemsArray = (state) => state.basket.basketItemsArray;
export const selectFullPrice = (state) => state.basket.fullPrice;
export default basketSlice.reducer;
