import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isAuth: false,
  user: null,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setIsAuth: (state, action) => {
      state.isAuth = action.payload;
    },
    setUser: (state, action) => {
      state.user = action.payload;
    },
  },

});

// Action creators are generated for each case reducer function
export const { setIsAuth, setUser } = userSlice.actions;
export const selectAuthStatus = (state) => state.user.isAuth;
export const selectUserData = (state) => state.user.user;
export default userSlice.reducer;
