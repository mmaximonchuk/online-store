import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../features/user/userSlice";
import deviceReducer from "../features/device/deviceSlice";
import basketReducer from "../features/basket/basketSlice";
import { filterMiddleware } from '../utils/filterMiddleware';

export const store = configureStore({
  reducer: {
    user: userReducer,
    device: deviceReducer,
    basket: basketReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(filterMiddleware),
});
