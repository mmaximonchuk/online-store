import React, { useCallback, useEffect, useState } from "react";
import { AiFillStar } from "react-icons/ai";
import { useParams } from "react-router-dom";
import { Button, Card, Col, Container, Image, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import cn from "classnames";

import { addItemToBasket } from "../http/basketApi";
import { fetchOneDevice, setDeviceRatingRequest } from "../http/deviceAPI";
import { useRequestBasketItems } from "../utils/useRequestBasketItems";
import { selectUserData } from "../features/user/userSlice";

import imgBigStar from "../assets/images/BigStar.svg";

import styles from "../styles/DevicePage.module.css";

function DevicePage() {
  const { id } = useParams();
  const user = useSelector(selectUserData);
  const [device, setDevice] = useState({ info: [] });
  const { refetch } = useRequestBasketItems(user);

  const [rating, setRating] = useState(Array(5).fill(<></>));
  const [currentRating, setCurrentRating] = useState(0);
  const [averageRating, setAverageRating] = useState(0);
  const [shoulRatingRerender, setRatingRerender] = useState(false);

  const handleAddToBasket = async (e, deviceId) => {
    e.stopPropagation();
    
    const userId = user?.id;
    if(!userId) return alert("Please authorize first!")
    await addItemToBasket(userId, deviceId);
    refetch();
  };

  const userCheckRating = useCallback(() => {
    // this condition checks does current user has already voted
    // and in case he did it sets the value as it has been voted
    if (device.ratings?.some((item) => item.userId === user?.id)) {
      setCurrentRating(
        device.ratings.find((item) => item.userId === user.id).rate
      );
    } else {
      setCurrentRating(averageRating);
    }
  }, [averageRating, device.ratings, user?.id]);

  const handleMouseEnter = useCallback((i) => {
    setCurrentRating(i + 1);
  }, []);

  const handleMouseLeave = useCallback(() => {
    userCheckRating();
  }, [userCheckRating]);

  const handleSetRatingRequest = useCallback(async () => {
    // makes a request to vote for the rating
    if (!user?.id) {
      return alert(
        "Only authenticated users can leave review!"
      );
    }

    const result = await setDeviceRatingRequest(
      device.id,
      user.id,
      currentRating
    );
    userCheckRating();
    setRatingRerender(!shoulRatingRerender);
    console.log("result: ", result);
  }, [
    currentRating,
    device.id,
    user?.id,
    shoulRatingRerender,
    userCheckRating,
  ]);

  const setAvarageDeviceRating = (ratingArray) => {
    // this functions comptutes the average rating of all votes
    // for the particular device
    if (ratingArray?.length) {
      const allRatings = ratingArray.reduce((acc, curr) => {
        return acc + curr.rate;
      }, 0);
      const avarageRating = Math.round(allRatings / ratingArray.length);
      setAverageRating(avarageRating);
    }
    return "";
  };

  const buildRating = useCallback(() => {
    const updatedArray = () =>
      rating.map((star, index) => (
        <button
          className={cn(styles.startContainer, {
            [styles.filled]: index < currentRating,
          })}
          onMouseEnter={() => handleMouseEnter(index)}
          onMouseLeave={() => handleMouseLeave()}
          onClick={() => handleSetRatingRequest()}
          type="button"
        >
          <AiFillStar />
        </button>
      ));
    setRating(updatedArray);
  }, [
    handleMouseEnter,
    handleMouseLeave,
    rating,
    handleSetRatingRequest,
    currentRating,
  ]);

  useEffect(() => {
    buildRating();
  }, [currentRating]);

  const requestDeviceData = async () => {
    try {
      const device = await fetchOneDevice(id);
      setDevice(device);
      userCheckRating();
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    requestDeviceData();
    buildRating();
  }, []);

  useEffect(() => {
    requestDeviceData();
    buildRating();
  }, [shoulRatingRerender]);

  useEffect(() => {
    setAvarageDeviceRating(device.ratings);
  }, [device.ratings]);

  useEffect(() => {
    userCheckRating();
  }, [averageRating, userCheckRating]);

  return (
    <Container className="mt-3">
      <Row>
        <Col md={4} className="mb-3">
          <Image
            style={{ objectFit: "scale-down" }}
            src={process.env.REACT_APP_API_URL + device.img}
            width="100%"
            height={300}
          />
        </Col>
        <Col md={4} className="mb-3">
          <Row className="d-flex align-items-center flex-column">
            <h2>{device.name}</h2>
            <div
              style={{
                background: `url(${imgBigStar}) no-repeat center center / contain`,
                height: "240px",
                width: "240px",
                fontSize: "48px",
              }}
              className="d-flex align-items-center justify-content-center"
            >
              {averageRating}
            </div>
            <h5>{`Total reviews: ${device?.ratings?.length ? device?.ratings?.length : 0}`}</h5>
          </Row>
        </Col>
        <Col md={4} className="mb-3">
          <Card
            className="d-flex align-items-center justify-content-between p-4"
            style={{
              margin: "0 auto",
              maxWidth: "300px",
              height: "300px",
              fontSize: 32,
              border: "5px solid lightgrey",
            }}
          >
            <h3>From: {device.price} $</h3>
            <Button onClick={(e) => handleAddToBasket(e, device.id)} variant={"outline-dark"} style={{ fontSize: 18 }}>
              Add to cart
            </Button>
          </Card>
        </Col>
      </Row>
      <Row className="col-md-4">
        <Col>
          <p className={styles.review}>
            {device.ratings?.some((rating) => rating.userId === user?.id)
              ? "Thank's for your review!"
              : "Leave your review:"}
          </p>
          <div className={styles.ratingContainer}>
            {rating.map((r, i) => (
              <span key={i}>{r}</span>
            ))}
          </div>
        </Col>
      </Row>
      <Row className="d-flex flex-column m-3">
        <Row className="mb-3">
          <h3 style={{ fontSize: 25 }}>Characteristics: </h3>
        </Row>
        {device.info.map((item, index) => {
          return (
            <Row
              key={item.id}
              style={{
                background: index % 2 === 0 ? "lightgrey" : "transparent",
                padding: 10,
              }}
            >
              {item.title} : {item.description}
            </Row>
          );
        })}
      </Row>
    </Container>
  );
}

export default DevicePage;
