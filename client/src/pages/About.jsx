import React from "react";
import { Col, Container, Row } from "react-bootstrap";

import s from "../styles/About.module.css";

export default function About() {
  return (
    <Container>
      <Row>
        <Col className="mt-3">
          <section className={s.section}>
            <p className={s.secTitle}>Contact ElectroPowers PC:</p>
            <p className={s.secText}>1.800.990.7945 (Mon~Fri, USA only)</p>
            <p className={s.secText}>
              Calling from outside of USA: 1.626.915.5008
            </p>
            <p className={s.secText}>
              email (for all inquires: sales question or support, RMA and
              return) :{" "}
              <a href="mailto:service@electropowers.com" target="_blank" rel="noopener noreferrer">
                service@electropowers.com
              </a>
            </p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>Our address:</p>
            <p className={s.secText}>ElectroPowers PC</p>
            <p className={s.secText}>1420 E. Cypress St</p>
            <p className={s.secText}>Covina, CA 91724</p>
            <p className={s.secText}>United States</p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>International order welcome</p>
            <p className={s.secText}>
              ElectroPowers ships internationally with different terms of
              payment method by country:
            </p>
            <p className={s.secText}>Credit Cards: Canada</p>
            <p className={s.secText}>PayPal: Worldwide</p>
            <p className={s.secText}>
              Wire Transfer: All countries outside of Canada and United Kingdom.
            </p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>Pre-Orders:</p>
            <p className={s.secText}>
              - ElectroPowers PC does not charge credit card for all pre-orders
              unless payment was sent from PayPal or per customers request to
              charge credit card first.
            </p>
            <p className={s.secText}>- Pre-Order can be cancel at any time.</p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>Returns:</p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>
              For Notebooks with stock configuration:
            </p>
            <p className={s.secText}>
              15 Days no questions asked full refund if the package is unopened.
            </p>
            <p className={s.secText}>
              - All notebooks can be returned for a refund with a 0% restocking
              fee if the package is unopened.
            </p>
            <p className={s.secText}>
              - 8% restocking fee will be apply to refund if the packaged has
              been opened.
            </p>
            <p className={s.secText}>
              - Shipping and handling are non-refundable.
            </p>
            <p className={s.secText}>
              - $35 will be deducted from refund if the product was shipped with
              free shipping.
            </p>
            <p className={s.secText}>
              - Customers are responsible for return shipping.
            </p>
            <p className={s.secText}>
              - Refunds must be requested within 15 days from the date the
              product was shipped. We do not accept refunds after 15 days from
              the date product was shipped.
            </p>
            <p className={s.secText}>
              - We do not accept refunds after 15 days from the date product was
              shipped.
            </p>
            <p className={s.secText}>
              - We must receive the product within 7 days from the date RMA
              number was issued, if we receive the product after 7 days there
              will be refused.
            </p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>Order cancellation prior shipping:</p>
            <p className={s.secText}>
              You may cancel the order prior order been shipped, however, we do
              deduct 6% credit card transaction fee.
            </p>
          </section>
          <hr className={s.customHr} />
          <section className={s.section}>
            <p className={s.secTitle}>Warranty</p>
            <p className={s.secText}>
              -All notebooks are covered by manufacturer's warranty except
              components upgraded by ElectroPowers.
            </p>
            <p className={s.secText}>
              -ElectroPowers components is not cover under manufacturer's
              accidental damage warranty if it's provided by manufacturer.
            </p>
            <p className={s.secText}>
              -All ElectroPowers parts are covered with a 1 year Limit parts
              warranty unless specified otherwise on special items. This
              coverage starts the day the item is shipped from our warehouse.
            </p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>Barebone Notebook Warranty</p>
            <p className={s.secText}>
              If components such as CPU, RAM, HD, Optical Drive etc. (exclude
              laptop itself) is DOA (Dead on Arrival) within first 30 day,
              ElectroPowers will send out replacement components by UPS Next Day
              Air at our cost when we receive the defective components,
              customers must call/email our tech support to troubleshoot. Please
              note, batteries has 1 year warranty only, extended warranty does
              not cover batteries.
            </p>
          </section>
          <hr className={s.customHr} />
          <section className={s.section}>
            <p className={s.secTitle}>RMA shipping insctruction</p>
            <p className={s.secText}>Use original manufacturer's packaging</p>
            <p className={s.secText}>
              All returns must be completed with all manuals, cables, warranty
              cards, static bags, just as you received it. If the product is not
              returned in its entirety, a fee will be charged for the missing
              items.
            </p>
            <p className={s.secText}>
              All returns must have an RMA number clearly referenced on shipping
              label (a copy of the invoice must be included).
            </p>
            <p className={s.secText}>
              To obtain a RMA number, please call our Service Department at
              1-626-915-5008 or E-Mail to service@ElectroPowerspc.com with the
              following: Invoice number, serial number, and problem description.
            </p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>What the warranty does not cover</p>
            <p className={s.secText}>
              Any product on which the serial number has been modified or
              removed. Damage or malfunction resulting from accident, misuse,
              power surge, fire, water, or other acts of nature, product
              modification, or failure to follow instructions supplied with the
              product. The alleged condition is a result of neglect,
              unauthorized or improper alternations, repairs, improper care,
              accidents, disaster, weather or transportation.
            </p>
            <p className={s.secTitle}>Replacement, Cross-Ship, Repair</p>
            <p className={s.secText}>
              Customer will need to issue a purchase order, use a credit card,
              or agree to pay by COD cash to entitle a cross shipment. Customer
              will need to return the defective parts for credit within seven
              days after the RMA issuance date.
            </p>
            <p className={s.secText}>
              ElectroPowers Computer does not cross ship RMA merchandise after
              thirty days.
            </p>
          </section>
          <section className={s.section}>
            <p className={s.secTitle}>Discrepancy & Shipping Damage</p>
            <p className={s.secText}>
              Any discrepancy and wrong items should be reported to the RMA
              department within seven working days. For shipping damages,
              customers should notify the carrier immediately and ask for the
              issuance of a damage report.
            </p>
            <p className={s.secText}>
              All lost and damaged packages must be investigated by shipping
              company, replacement items will not be ship unless funds has been
              paid by insurance.
            </p>
          </section>
        </Col>
      </Row>
    </Container>
  );
}
