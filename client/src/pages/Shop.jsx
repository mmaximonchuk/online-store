import React, { useEffect } from "react";
import { useLocationField } from "react-location-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { Col, Container, Row } from "react-bootstrap";

import TypeBar from "../components/TypeBar";
import BrandBar from "../components/BrandBar";
import DeviceList from "../components/DeviceList";
import Pages from "../components/Pages";

// import { getBasketData } from "../http/basketApi";
import { fetchBrands, fetchTypes, fetchDevices } from "../http/deviceAPI";
// import { selectAuthStatus, selectUserData } from "../features/user/userSlice";
import {
  setTypes,
  setBrands,
  setDevices,
  setPage,
  setTotalCount,
  selectPage,
  selectLimit,
  selectFilteredBrand,
  selectFilteredType,
  setFilteredType,
  setFilteredBrand,
  selectBrands,
  selectTypes,
  selectDevices,
} from "../features/device/deviceSlice";

function Shop() {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  // const isAuthUser = useSelector(selectAuthStatus);
  const currentPage = useSelector(selectPage);
  const limit = useSelector(selectLimit);
  const types = useSelector(selectTypes);
  const brands = useSelector(selectBrands);
  const devices = useSelector(selectDevices);
  const filteredType = useSelector(selectFilteredType);
  const filteredBrand = useSelector(selectFilteredBrand);
  const [fPage, setFilterPage] = useLocationField("page", {
    type: "number",
    initial: 1,
    onParsedError: () => 1,
  });
  const [fBrand, setFilterBrand] = useLocationField("brandId", "");
  const [fType, setFilterType] = useLocationField("typeId", "");

  const requestShopData = async (fPage, fBrand, fType) => {
    try {
      const types = await fetchTypes();
      const brands = await fetchBrands();
      const devices = await fetchDevices(
        fType.length ? fType : null,
        fBrand.length ? fBrand : null,
        fPage,
        limit
      );

      dispatch(setTypes(types));
      dispatch(setBrands(brands));
      dispatch(setDevices(devices));
      dispatch(setTotalCount(devices.count));

      dispatch(setFilteredType(types.find((type) => type.id == fType) || {}));
      dispatch(
        setFilteredBrand(brands.find((brand) => brand.id == fBrand) || {})
      );
      dispatch(setPage(fPage));
    } catch (err) {
      console.log(err.message);
    }
  };

  useEffect(() => {
    console.log(fType);
    console.log(fBrand);
    (async () => {
      try {
        const devices = await fetchDevices(
          fType !== undefined ? fType : filteredType?.id,
          fBrand !== undefined ? fBrand : filteredBrand?.id,
          currentPage,
          limit
        );
        dispatch(setDevices(devices));
        dispatch(setTotalCount(devices.count));
      } catch (err) {
        console.log(err.message);
      }
    })();
  }, [currentPage, filteredType, filteredBrand]);

  useEffect(() => {
    requestShopData(fPage, fBrand, fType);
  }, []);

  useEffect(() => {
    if (types.length > 0) 
      dispatch(setFilteredType(types.find((type) => type.id == fType) || {}));
   
  }, [types]);

  useEffect(() => {
    if (brands.length > 0) 
      dispatch(
        setFilteredBrand(brands.find((brand) => brand.id == fBrand) || {})
      );
    
  }, [brands]);

  useEffect(() => {
    setFilterPage(currentPage);
  }, [currentPage]);

  useEffect(() => {
    dispatch(setFilteredType(types.find((type) => type?.id == fType) ?? {}));

    if (devices.rows !== undefined && devices.rows.length / limit < currentPage)
      dispatch(setPage(1));
  }, [fType]);

  useEffect(() => {
    dispatch(
      setFilteredBrand(brands.find((brand) => brand?.id == fBrand) ?? {})
    );

    if (devices.rows !== undefined && devices.rows.length / limit < currentPage)
      dispatch(setPage(1));
  }, [fBrand]);

  useEffect(() => {
    setFilterType(filteredType.id ?? "");
  }, [filteredType]);

  useEffect(() => {
    setFilterBrand(filteredBrand.id ?? "");
  }, [filteredBrand]);

  return (
    <Container>
      <Row>
        <Col md={3} className="mb-3">
          <TypeBar />
        </Col>
        <Col md={9}>
          <BrandBar
            setFilterBrand={setFilterBrand}
            requestShopData={requestShopData}
            setFilterType={setFilterType}
            setFilterPage={setFilterPage}
          />
          <DeviceList />
          <Pages />
        </Col>
      </Row>
    </Container>
  );
}

export default Shop;
