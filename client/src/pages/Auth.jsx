import React, { useState } from "react";
import { NavLink, useHistory, useLocation } from "react-router-dom";
import { Button, Card, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";

import {
  REGISTRATION_ROUTE,
  LOGIN_ROUTE,
  SHOP_ROUTE,
} from "../utils/constants";
import {
  setIsAuth,
  setUser,
} from "../features/user/userSlice";
import { login, registration } from "../http/userAPI";

function Auth() {
  const history = useHistory();
  // const user = useSelector(selectUserData);
  // const isAuth = useSelector(selectAuthStatus);
  const dispatch = useDispatch();
  const location = useLocation();
  const isLogin = location.pathname === LOGIN_ROUTE;
  const [formData, setFormData] = useState({ email: "", password: "" });

  const handleForms = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const authLogin = async (e) => {
    e.preventDefault();
    try {
      let user;
      if (isLogin) {
        user = await login(formData.email, formData.password);
        // console.log("response LOGIN: ", response);
      } else {
        // console.log(formData.email, formData.password);
        user = await registration(formData.email, formData.password);
        // console.log("response REGISTER: ", response);
      }
      dispatch(setUser(user));
      dispatch(setIsAuth(true));
      history.push(SHOP_ROUTE);
    } catch (err) {
      alert(err.response.data.message);
    }
  };

  return (
    <Container
      className="d-flex align-items-center justify-content-center"
      style={{ height: window.innerHeight - 54 }}
    >
      <Card style={{ width: 600 }} className="p-5">
        <h2 className="m-auto">{isLogin ? "Авторизация" : "Регистрация"}</h2>
        <Form onSubmit={authLogin} className="d-flex flex-column">
          <Form.Control
            onChange={handleForms}
            name="email"
            value={formData.email}
            className="mt-3"
            placeholder="Введите ваш e-mail..."
          />
          <Form.Control
            onChange={handleForms}
            name="password"
            value={formData.password}
            type="password"
            className="mt-3"
            placeholder="Введите ваш пароль..."
          />
          <Row className="d-flex align-items-center justify-content-between mt-3 pl-3 pr-3">
            {isLogin ? (
              <div>
                Нет аккаунта?{" "}
                <NavLink to={REGISTRATION_ROUTE}>Зарегестрируйтесь!</NavLink>
              </div>
            ) : (
              <div>
                Уже есть аккаунт? <NavLink to={LOGIN_ROUTE}>Войдите!</NavLink>
              </div>
            )}
            <Button
              type="submit"
              className="align-self-end"
              variant={"outline-success"}
            >
              {isLogin ? "Sign In" : "Sign Up"}
            </Button>
          </Row>
        </Form>
      </Card>
    </Container>
  );
}

export default Auth;
