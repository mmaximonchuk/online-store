import React from "react";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { Button, Container, Row, Col, Spinner } from "react-bootstrap";
import { FaMinus, FaPlus } from "react-icons/fa";
import { GrClose } from "react-icons/gr";

import {
  addItemToBasket,
  removeItemFromBasket,
  removeSingleItemFromBasket,
} from "../http/basketApi";
import { useRequestBasketItems } from "../utils/useRequestBasketItems";
import { CONFIRM_ROUTE } from "../utils/constants";
import {
  removeBasketItem,
  selectBasketData,
  selectBasketItemsArray,
  selectFullPrice,
} from "../features/basket/basketSlice";
import { selectUserData } from "../features/user/userSlice";

import styles from "../styles/Basket.module.css";

function Basket() {
  const dispatch = useDispatch();
  const history = useHistory();
  const basketData = useSelector(selectBasketData);
  const basketItemsArray = useSelector(selectBasketItemsArray);
  const fullPrice = useSelector(selectFullPrice);
  const user = useSelector(selectUserData);
  const { refetch } = useRequestBasketItems(user);

  const handleRemoveAll = async (deviceId) => {
    await removeItemFromBasket(user.id, deviceId);

    dispatch(removeBasketItem(deviceId));
    refetch();
  };

  const handleRemoveSingle = async (e, deviceId, itemId) => {
    console.log("itemId: ", itemId);
    e.stopPropagation();

    await removeSingleItemFromBasket(user.id, itemId);

    dispatch(removeBasketItem(deviceId));
    refetch();
  };
  const handleAddToBasket = async (e, deviceId) => {
    e.stopPropagation();

    const userId = user?.id;
    if (!userId) return alert("Please authorize first!");
    await addItemToBasket(userId, deviceId);
    refetch();
  };

  // useEffect(() => {
  // startBasketRequest(user)
  // ! == ====== ====it was a wrong road to nowhere!!! ==== == == ==== !
  // if (basketData.basket_devices !== undefined) {
  // (async () => {
  //   // request each basket item paralelly
  //   Promise.all(
  //     basketData.basket_devices.map(
  //       async (device) => {
  //        const result = await fetchOneDevice(device.deviceId)
  //        dispatch(addBasketItem(result));
  //       }
  //     )
  //   );
  // })();
  // }
  // ! == ====== ====it was a wrong road to nowhere!!! ==== == == ==== !
  // }, []);

  return (
    <div className={styles.basket}>
      <Container className="pt-3">
        <Row>
          <Col md={{ span: 8, order: 1 }} xs={{ span: 12, order: 2 }}>
            <div className={styles.itemContainer}>
              {basketData?.basket_devices && basketItemsArray ? (
                basketItemsArray.map((basketItem, index) => (
                  <BasketItem
                    key={basketItem.id}
                    handleRemoveAll={handleRemoveAll}
                    handleAddToBasket={handleAddToBasket}
                    handleRemoveSingle={handleRemoveSingle}
                    basketData={basketData}
                    {...basketItem}
                  />
                ))
              ) : (
                <Spinner size="3rem" />
              )}
              {basketData?.basket_devices?.length === 0 && (
                <h4>Your cart is empty</h4>
              )}
            </div>
          </Col>
          <Col md={{ span: 4, order: 2 }} xs={{ span: 12, order: 1 }}>
            <div className={styles.basketInfo}>
              Full order price:
              <span className={styles.fullPrice}>{fullPrice} $</span>
              <Button
                onClick={() => history.push(CONFIRM_ROUTE)}
                variant={"outline-primary"}
              >
                Confirm order
              </Button>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Basket;

const BasketItem = (props) => {
  // const { motion } = Framer;
  const basketCell =
    props.basketData.basket_devices.find(
      (item) => item.deviceId === props.id
    ) || 0;
  return (
    <div className={styles.item}>
      <div className={styles.img}>
        <img src={`${process.env.REACT_APP_API_URL}/${props.img}`} alt="" />
      </div>
      <div className={styles.itemData}>
        <p>{props.name}</p>
        <div className={styles.quantityWrapper}>
          <button
            onClick={(e) =>
              props.handleRemoveSingle(e, props.id, basketCell.id)
            }
            className={styles.changeCountBtn}
          >
            <FaMinus />
          </button>
          <input type="text" className={styles.input} value={props.count} />
          <button
            onClick={(e) => props.handleAddToBasket(e, props.id)}
            className={styles.changeCountBtn}
          >
            <FaPlus />
          </button>
        </div>
        <h4>{props.price} $</h4>
      </div>
      <div className={styles.remove}>
        <button
          onClick={() => props.handleRemoveAll(props.id)}
          className={styles.removeBtn}
        >
          <GrClose />
        </button>
      </div>
    </div>
  );
};
