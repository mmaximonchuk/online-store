import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  InputGroup,
  FormControl,
  Button,
  Dropdown,
} from "react-bootstrap";
import { fetchCountries } from "../http/countriesAPI";

import s from "../styles/Confirm.module.css";

export default function Confirm() {
  const [countries, setCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState({});
	const [formData, setFormData] = useState({});
  const [countriesErr, setCountriesErr] = useState([""]);

	const handleChangeFormData = (e) => {
		const name = e.target.name;
		const value = e.target.value;
		setFormData({ ...formData, [name]: value })
	}
  
	// useEffect(() => {
	// 	console.log(formData);
	// }, [formData]);

  const getCountries = async () => {
    try {
      const result = await fetchCountries();
      setCountries(result);
    } catch (e) {
      setCountriesErr(e);
    }
  };

  useEffect(() => {
    getCountries();
  }, []);


  return (
    <Container>
      <Row>
        <Col>
          <h1 className={s.title}>Confirm your order</h1>
        </Col>
      </Row>
      <Row>
        <Col className="mt-3 justify-content-center">
          <form action="" className={s.form}>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>First name:</span>
                <FormControl
                  name="firstName"
                  placeholder="First name"
                  aria-label="First name"
                  value={formData?.firstName || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>Last name:</span>
                <FormControl
                  name="lastName"
                  placeholder="Last name"
                  aria-label="Last name"
                  value={formData?.lastName || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>Company:</span>
                <FormControl
                  name="company"
                  placeholder="Company"
                  aria-label="Company"
                  value={formData?.company || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>Adress:</span>
                <FormControl
                  name="adress"
                  placeholder="Adress"
                  aria-label="Adress"
                  value={formData?.adress || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>City:</span>
                <FormControl
                  name="city"
                  placeholder="City"
                  aria-label="City"
                  value={formData?.city || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>Country:</span>
                <Dropdown className={s.dropDown}>
                  <Dropdown.Toggle className={s.cDropdown} id="dropdown-basic">
                    {selectedCountry?.name?.official?.length
                      ? selectedCountry?.name?.official
                      : "Countries"}
                  </Dropdown.Toggle>

                  <Dropdown.Menu className={s.dropdownMenu}>
                    {countries.length &&
                      countries.map((country) => (
                        <Dropdown.Item
                          onClick={() => setSelectedCountry(country)}
                          key={country.name.official}
                        >
                          {country.name.official}
                        </Dropdown.Item>
                      ))}
                  </Dropdown.Menu>
                </Dropdown>
                {/* <FormControl placeholder="Country" aria-label="Country" /> */}
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>State/Province:</span>
                <FormControl
                  name="state"
                  placeholder="State/Province"
                  aria-label="State"
                  value={formData?.state || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>Zip/Postal Code:</span>
                <FormControl
                  name="postalCode"
                  placeholder="Zip/Postal Code"
                  aria-label="Zip/Postal Code"
                  value={formData?.postalCode || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <InputGroup className={`${s.inputGroup} mb-3`}>
              <label className={s.label}>
                <span className={s.labelSpan}>Phone number:</span>
                <FormControl
                  name="phoneNumber"
                  placeholder="Phone number"
                  aria-label="Phone number"
                  value={formData?.phoneNumber || ""}
                  onChange={handleChangeFormData}
                />
              </label>
            </InputGroup>
            <Button
              variant="primary"
              className="btn btn-success align-self-center"
            >
              Confirm
            </Button>
          </form>
        </Col>
      </Row>
    </Container>
  );
}
