// import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { fetchBasketDevices, getBasketData } from "../http/basketApi";
// import { fetchOneDevice } from "../http/deviceAPI";

import {
  setBasketData,
  setBasketDevices,
} from "../features/basket/basketSlice";

export function useRequestBasketItems(user) {
  // const [requestBasketItems, setRequestBasketItems] = useState(false);
  const dispatch = useDispatch();

  const refetch = async () => {
    if (!(user?.id >= 0)) return;
    if (!user.id)
      return alert(
        "Добавлять покупки могут только зарегестрированые пользователи!"
      );
    try {
      const data = await getBasketData(user.id);

      dispatch(setBasketData(data));
      const devices = await fetchBasketDevices(user.id);
      dispatch(setBasketDevices(devices));
    } catch (err) {
      console.log(err.message);
    }
  };

  // useEffect(() => {
  //   if (user?.id >= 0) {
  //     request(user.id);
  //   }
  // }, [requestBasketItems]);

  return { refetch };
}
