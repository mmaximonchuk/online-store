import {
  setFilteredBrand,
  setFilteredType,
  setPage,
} from "../features/device/deviceSlice";


export const filterMiddleware = (store) => (next) => (action) => {
  if (setFilteredType.match(action)) {
    localStorage.setItem("fType", JSON.stringify(action.payload));
  } else if (setFilteredBrand.match(action)) {
    localStorage.setItem("fBrand", JSON.stringify(action.payload));
  } else if (setPage.match(action)) {
    // if(window.location.search.includes("page")) {
    //   // const regExp = /(?!\?page=)\d+/g;
    //   // window.location.search.replace(regExp, action.payload);

    // } else {
    //   // window.location.search = `?page=${action.payload}`;

    // }

    localStorage.setItem("fPage", JSON.stringify(action.payload));
  }
  return next(action);
};
