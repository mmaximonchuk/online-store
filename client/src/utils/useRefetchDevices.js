import { useDispatch } from "react-redux";
import { fetchDevices } from "../http/deviceAPI";

import { setDevices } from '../features/device/deviceSlice';

export function useRefetchDevices() {
  const dispatch = useDispatch();

  const refetchDevices = async () => {
    try {
      const devices = await fetchDevices();
      dispatch(setDevices(devices));
    } catch (err) {
      console.log(err.message);
    }
  };

  return { refetchDevices };
}
