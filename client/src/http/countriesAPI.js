// https://api.first.org/data/v1/countries

import { $authHost } from "./index";

export const fetchCountries = async () => {
  const { data } = await $authHost.get("https://restcountries.com/v3.1/all");
	return data
}