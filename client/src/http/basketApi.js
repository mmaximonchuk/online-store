import { $authHost } from "./index";

export const addItemToBasket = async (userId = null, deviceId) => {
	  const { data } = await $authHost.post("api/basket", { userId, deviceId });
    return data;
};

export const getBasketData = async (userId) => {
  const { data } = await $authHost.get(`api/basket/${userId}`);
  return data;
};

export const fetchBasketDevices = async (userId) => {
  const { data } = await $authHost.get(`api/basket?userId=${userId}`);
  return data;
};

export const removeItemFromBasket = async (userId, deviceId) => {
  const { data } = await $authHost.put("api/basket", { userId, deviceId });
  return data;
};
export const removeSingleItemFromBasket = async (userId, itemId) => {
  const { data } = await $authHost.patch("api/basket", { userId, itemId });
  return data;
};
