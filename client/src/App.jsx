import React, { useEffect, useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { BrowserLocationQuery } from "react-location-query";
import { Spinner } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

import Navbar from "./components/Navbar";
import AppRouter from "./components/AppRouter";
import Footer from "./components/Footer";

import { setUser, setIsAuth, selectUserData } from "./features/user/userSlice";
import { check } from "./http/userAPI";
import { useRequestBasketItems } from "./utils/useRequestBasketItems";

import "./global.css";
// import { getBasketData } from "./http/basketApi";
// import { setBasketData } from "./features/basket/basketSlice";

function App() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const user = useSelector(selectUserData);
  const { refetch } = useRequestBasketItems(user);

  useEffect(() => {
    (async () => {
      try {
        const result = await check();
        dispatch(setUser(result));
        dispatch(setIsAuth(true));
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    refetch();
  }, [user, dispatch]);

  if (loading) {
    return (
      <Spinner
        animation={"border"}
        size="md"
        style={{
          width: "4rem",
          height: "4rem",
          position: "fixed",
          top: "48%",
          left: "48%",
        }}
      />
    );
  }

  return (
    <Router>
      <BrowserLocationQuery>
        <Navbar />
        <AppRouter />
        <Footer />
      </BrowserLocationQuery>
    </Router>
  );
}

export default App;
