import Admin from './pages/Admin';
import Basket from './pages/Basket';
import Shop from './pages/Shop';
import DevicePage from './pages/DevicePage';
import Auth from './pages/Auth';
import {
  ADMIN_ROUTE,
  LOGIN_ROUTE,
  REGISTRATION_ROUTE,
  SHOP_ROUTE,
  BASKET_ROUTE,
  DEVICE_ROUTE,
  ABOUT_ROUTE,
  CONFIRM_ROUTE,
  PRIVACY_ROUTE,
  TERMS_ROUTE,
} from "./utils/constants";
import About from './pages/About';
import Confirm from './pages/Confirm';
import PrivacyPolicy from './pages/PrivacyPolicy';
import Terms from './pages/Terms';
export const authRoutes = [
    {
        path: BASKET_ROUTE,
        Component: Basket,
    },
];
export const adminRoutes = [
  {
    path: ADMIN_ROUTE,
    Component: Admin,
  },
];
export const publicRoutes = [
  {
    path: LOGIN_ROUTE,
    Component: Auth,
  },
  {
    path: REGISTRATION_ROUTE,
    Component: Auth,
  },
  {
    path: SHOP_ROUTE,
    Component: Shop,
  },
  {
    path: DEVICE_ROUTE + "/:id",
    Component: DevicePage,
  },
  {
    path: ABOUT_ROUTE,
    Component: About,
  },
  {
    path: CONFIRM_ROUTE,
    Component: Confirm,
  },
  {
    path: PRIVACY_ROUTE,
    Component: PrivacyPolicy,
  },
  {
    path: TERMS_ROUTE,
    Component: Terms,
  },
];