import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { ABOUT_ROUTE, PRIVACY_ROUTE } from '../utils/constants';
import s from "../styles/Footer.module.css";

function Footer() {
	return (
    <footer className={s.footer}>
      <Container>
        <Row>
          <Col>
            <NavLink className={s.navLink} to={PRIVACY_ROUTE}>
              Privacy Policy
            </NavLink>
            <NavLink className={s.navLink} to={ABOUT_ROUTE}>
              Contact / Support
            </NavLink>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}

export default Footer;
