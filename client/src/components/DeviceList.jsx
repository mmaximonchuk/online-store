import React from "react";
import { Col, Row, Spinner } from "react-bootstrap";
import { useSelector } from "react-redux";
import { selectDevices } from "../features/device/deviceSlice";
import DeviceItem from "./DeviceItem";
function DeviceList() {
  //   const [selectedDevice, setSelectedDevice] = useState({});
  const devices = useSelector(selectDevices);

  if (devices?.message?.length) {
    return (
      <Row className="mt-3 d-flex">
        <Col>
          <p style={{ fontSize: "22px", textAlign: "center" }}>
            {devices.message}
          </p>
        </Col>
      </Row>
    );
  }

  return (
    <Row className="mt-3 d-flex">
      {devices?.rows?.length ? (
        devices.rows.map((device) => (
          <DeviceItem key={device.id} device={device} />
        ))
      ) : (
        <Col>
          <Spinner animation={"border"} />
        </Col>
      )}
    </Row>
  );
}

export default DeviceList;
