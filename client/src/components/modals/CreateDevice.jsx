import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Dropdown, Form, Modal, Row } from "react-bootstrap";
import PropTypes from 'prop-types';
import {
  selectBrands,
  selectTypes,
  setBrands,
  setDevices,
  setTypes,
} from "../../features/device/deviceSlice";
import {
  createDevice,
  fetchBrands,
  fetchDevices,
  fetchTypes,
} from "../../http/deviceAPI";
import DropdownItem from "react-bootstrap/esm/DropdownItem";

function CreateDevice({ show, onHide }) {
  const dispatch = useDispatch();
  const [info, setInfo] = useState([]);
  const [formData, setFormData] = useState({});
  const [selectedType, setSelectedType] = useState(null);
  const [selectedBrand, setSelectedBrand] = useState(null);
  const [file, setFile] = useState(null);
  const brands = useSelector(selectBrands);
  const types = useSelector(selectTypes);

  const addInfo = () => {
    setInfo([
      ...info,
      { title: "", description: "", number: new Date().getMilliseconds() },
    ]);
  };

  const addFile = (e) => setFile(e.target.files[0]);

  const removeProperty = (propertyId) => {
    setInfo(info.filter((property) => property.number !== propertyId));
  };
  const changeInfo = (key, value, number) => {
    setInfo(
      info.map((i) => (i.number === number ? { ...i, [key]: value } : i))
    );
  };
  const handleFormData = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormData({ ...formData, [name]: value });
  };

  const addDevice = async () => {
    const newDeviceformData = new FormData();
    newDeviceformData.append("name", formData.name);
    newDeviceformData.append("price", `${formData.price}`);
    newDeviceformData.append("img", file);
    newDeviceformData.append("brandId", selectedBrand.id);
    newDeviceformData.append("typeId", selectedType.id);
    newDeviceformData.append("info", JSON.stringify(info));
    const response = await createDevice(newDeviceformData);
    console.log(response);
    onHide();
  };

  useEffect(() => {
    (async () => {
      try {
        const types = await fetchTypes();
        const brands = await fetchBrands();
        const devices = await fetchDevices();
        dispatch(setTypes(types));
        dispatch(setBrands(brands));
        dispatch(setDevices(devices));
      } catch (err) {
        console.log(err.message);
      }
    })();
  }, []);

  return (
    <Modal size="lg" centered show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Добавить устройство
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Dropdown className="mt-2">
            <Dropdown.Toggle>
              {selectedType?.name || "Выберите тип"}
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {types.map((type) => {
                return (
                  <Dropdown.Item
                    onClick={() => setSelectedType(type)}
                    name={type.name}
                    key={type.id}
                  >
                    {type.name}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
          <Dropdown className="mt-2">
            <Dropdown.Toggle>
              {selectedBrand?.name || "Выберите брэнд"}
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {brands.map((brand) => {
                return (
                  <Dropdown.Item
                    onClick={() => setSelectedBrand(brand)}
                    key={brand.id}
                  >
                    {brand.name}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
          <Form.Control
            placeholder="Введите название устройства"
            className="mt-3"
            name="name"
            onChange={handleFormData}
          />
          <Form.Control
            placeholder="Введите стоимость устройства"
            type="number"
            className="mt-3"
            name="price"
            onChange={handleFormData}
          />
          <Form.Control onChange={addFile} type="file" className="mt-3" />
          {/* <hr/> */}
          <Button onClick={addInfo} className="mt-3" variant={"outline-dark"}>
            Добавить новое свойство
          </Button>
          {info.map((i) => {
            return (
              <Row key={i.number} className="mt-4">
                <Col md={4} className="mb-2">
                  <Form.Control
                    type="text"
                    value={i.title}
                    onChange={(e) =>
                      changeInfo("title", e.target.value, i.number)
                    }
                    placeholder="Введите название свойства"
                  />
                </Col>
                <Col md={4} className="mb-2">
                  <Form.Control
                    type="text"
                    value={i.description}
                    onChange={(e) =>
                      changeInfo("description", e.target.value, i.number)
                    }
                    placeholder="Введите описание свойства"
                  />
                </Col>
                <Col md={4}>
                  <Button
                    onClick={() => removeProperty(i.number)}
                    variant={"outline-danger"}
                  >
                    Удалить
                  </Button>
                </Col>
              </Row>
            );
          })}
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={"outline-danger"} onClick={onHide}>
          Закрыть
        </Button>
        <Button variant={"outline-success"} onClick={addDevice}>
          Добавить
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CreateDevice;
CreateDevice.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
};