import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import PropTypes from 'prop-types';
import { createBrand } from '../../http/deviceAPI';

function CreateBrand({show, onHide}) {
  const [value, setValue] = useState("");
  const addBrand = async () => {
    try {
      await createBrand({ name: value });
      setValue("");
      onHide();
    } catch (e) {
      console.log(e.message);
    }
  };
    return (
        <Modal size="lg" centered show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Добавить бренд
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Control value={value} onChange={(e) => setValue(e.target.value)}placeholder="Введите название бренда" />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant={'outline-danger'} onClick={onHide}>Закрыть</Button>
          <Button variant={'outline-success'} onClick={addBrand}>Добавить</Button>
        </Modal.Footer>
      </Modal>
    )
}

export default CreateBrand
CreateBrand.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
};
