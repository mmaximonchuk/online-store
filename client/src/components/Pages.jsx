import React, { useEffect } from "react";
import { Pagination } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import {
  selectDevices,
  selectFilteredBrand,
  selectFilteredType,
  selectLimit,
  selectPage,
  selectTotalCount,
  setPage,
} from "../features/device/deviceSlice";

function Pages() {
  const dispatch = useDispatch();
  const devices = useSelector(selectDevices);
  const currentPage = useSelector(selectPage);
  const filteredType = useSelector(selectFilteredType);
  const filteredBrand = useSelector(selectFilteredBrand);
  const totalCount = useSelector(selectTotalCount);
  const limit = useSelector(selectLimit);

  const pagesCount = Math.ceil(totalCount / limit);
  const pages = [];

  for (let i = 0; i < pagesCount; i++) {
    pages.push((i + 1));
  }

  useEffect(() => {
    const pagesCount = Math.ceil(totalCount / limit);

    for (let i = 0; i < pagesCount; i++) {
      pages.push(i + 1);
    }
    
  }, [devices, filteredType, filteredBrand, limit, totalCount, pages]);



  return (
    <Pagination className="mt-3 justify-content-center">
      {pages.map((page) => {
        return (
          <Pagination.Item
            key={page}
            active={currentPage === page}
            onClick={() => dispatch(setPage(page))}
          >
            {page}
          </Pagination.Item>
        );
      })}
    </Pagination>
  );
}

export default Pages;

// Pages.propTypes = {
//   selectedType: PropTypes.object.isRequired,
//   selectedBrand: PropTypes.object.isRequired
// };