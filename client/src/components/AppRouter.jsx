import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import { adminRoutes, authRoutes, publicRoutes } from "../routes";
import { SHOP_ROUTE } from "../utils/constants";
import { selectAuthStatus, selectUserData } from "../features/user/userSlice";

function AppRouter() {
  const isAuth = useSelector(selectAuthStatus);
  const user = useSelector(selectUserData);
  
  return (
    <div
      style={{
        backgroundColor: "rgb(245 245 245)",
        minHeight: "100vh",
        paddingTop: "20px",
      }}
      className="appContainer"
    >
      <Switch>
        {isAuth &&
          authRoutes.map(({ path, Component }) => {
            return <Route key={path} path={path} component={Component} exact />;
          })}
        {user?.role === "ADMIN" &&
          adminRoutes.map(({ path, Component }) => {
            return <Route key={path} path={path} component={Component} exact />;
          })}
        {publicRoutes.map(({ path, Component }) => {
          return <Route key={path} path={path} component={Component} exact />;
        })}
        <Redirect to={SHOP_ROUTE} />
      </Switch>
    </div>
  );
}

export default AppRouter;
