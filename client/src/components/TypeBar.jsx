import React, { useState } from "react";
import { ListGroup } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
// import PropTypes from "prop-types";

import { selectFilteredType, selectTypes, setFilteredType } from "../features/device/deviceSlice";

function TypeBar() {
  const dispatch = useDispatch();
  const types = useSelector(selectTypes);
  const filterTypes = useSelector(selectFilteredType);

  const handleTypeSelect = (type) => {
    dispatch(setFilteredType(type));
  }

  return (
    <ListGroup>
      {types.map((type) => {
        return (
          <ListGroup.Item
            key={type.id}
            style={{ cursor: "pointer" }}
            active={type.id === filterTypes.id}
            onClick={() => handleTypeSelect(type)}
          >
            {type.name}
          </ListGroup.Item>
        );
      })}
    </ListGroup>
  );
}

export default TypeBar;
// TypeBar.defaultProps = {
//   setSelectedType: () => {}
// };

// TypeBar.propTypes = {
//   selectedType: PropTypes.object.isRequired,
//   setSelectedType: PropTypes.func.isRequired,
// };