import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Button, Card, Col } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";

import { selectBrands } from "../features/device/deviceSlice";
import { selectUserData } from "../features/user/userSlice";

import { addItemToBasket } from "../http/basketApi";
import { deleteDevice } from "../http/deviceAPI";
import { DEVICE_ROUTE } from "../utils/constants";

import imgRatingStar from "../assets/images/BigStar.svg";
import { useRequestBasketItems } from "../utils/useRequestBasketItems";
import { useRefetchDevices } from '../utils/useRefetchDevices';

function DeviceItem({ device }) {
  const history = useHistory();
  const dispatch = useDispatch();
  const [deviceBrand, setDeviceBrand] = useState("");
  const [averageRating, setAverageRating] = useState(0);
  const brands = useSelector(selectBrands);
  const user = useSelector(selectUserData);
  // const devices = useSelector(selectDevices);
  const { refetch } = useRequestBasketItems(user);
  const {refetchDevices} = useRefetchDevices()
  const setAvarageDeviceRating = (ratingArray) => {
    if (ratingArray?.length) {
      const allRatings = ratingArray.reduce((acc, curr) => {
        return acc + curr.rate;
      }, 0);
      const avarageRating = Math.round(allRatings / ratingArray.length);
      setAverageRating(avarageRating);
    }
    return "";
  };

  const handleAddToBasket = async (e, deviceId) => {
    e.stopPropagation();
    
    const userId = user?.id;
    if (!userId) return alert("Please authorize first!");
    await addItemToBasket(userId, deviceId);
    refetch();
  };

  const handleDelete = async (e, deviceId) => {
    e.stopPropagation();
    try {
    const result = await deleteDevice(deviceId);
    refetchDevices();
    } catch (e) {
      console.error(e.message)
    }
  }

  useEffect(() => {
    setDeviceBrand(brands.find((brand) => device.brandId === brand.id));
  }, [device, brands]);

  useEffect(() => {
    setAvarageDeviceRating(device.ratings);
  }, [device]);

  return (
    <Col sm={6} md={4} lg={3}>
      <Card
        className="p-3 mb-3"
        style={{ cursor: "pointer", height: "calc(100% - 16px)" }}
        onClick={() => history.push(DEVICE_ROUTE + "/" + device.id)}
      >
        {user?.role === "ADMIN" && <Button onClick={(e) => handleDelete(e, device.id)}style={{position: 'absolute', right: 5, top: 5}} variant="danger">X</Button>}
        <Card.Img
          variant="top"
          width="100%"
          height={150}
          style={{ objectFit: "scale-down" }}
          src={`${process.env.REACT_APP_API_URL}/${device.img}`}
        />
        <div className="d-flex align-items-center justify-content-between mt-2">
          <div
            style={{ fontSize: "14px" }}
            className="item-description text-black-50"
          >
            {deviceBrand?.name}
          </div>
          <div className="d-flex align-items-center rating">
            <span style={{ fontSize: "16px" }} className="mr-1">
              {averageRating}
            </span>
            <img
              style={{ height: "16px", width: "16px" }}
              src={imgRatingStar}
              alt=""
            />
          </div>
        </div>
        <div
          style={{ fontSize: "16px", minHeight: "48px" }}
          className="mt-1 item-name"
        >
          {device.name}
        </div>
        <h4
          style={{
            fontSize: "22px",
            color: "rgb(15 61 146)",
            fontWeight: "bold",
            marginBottom: "12px",
          }}
          className="mt-1 item-name"
        >
          {device.price} грн
        </h4>
        <Button
          onClick={(e) => handleAddToBasket(e, device.id)}
          style={{ fontSize: "13px" }}
          variant={"outline-primary"}
        >
          Добавить в корзину
        </Button>
      </Card>
    </Col>
  );
}

export default DeviceItem;
DeviceItem.propTypes = {
  device: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
    ratings: PropTypes.array,
    img: PropTypes.string,
    createdAt: PropTypes.string,
    updatedAt: PropTypes.string,
    id: PropTypes.number,
    typeId: PropTypes.number,
    brandId: PropTypes.number,
  }),
};
