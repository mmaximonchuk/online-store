import React from "react";
import { HiRefresh } from "react-icons/hi";
import { Card, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from 'prop-types';

import { selectBrands, selectFilteredBrand, setFilteredBrand, setFilteredType, setPage } from "../features/device/deviceSlice";

import styles from "../styles/BrandBar.module.css";

function TypeBar({
  requestShopData,
  setFilterBrand,
  setFilterType,
  setFilterPage,
}) {
  const dispatch = useDispatch();
  const brands = useSelector(selectBrands);
  const filteredBrand = useSelector(selectFilteredBrand);

  const handleFiltersReset = () => {
    requestShopData();
    setFilterBrand("");
    setFilterType("");
    setFilterPage(1);
    dispatch(setPage(1));
  };

  return (
    <Row className={`pl-3 pr-3 ${styles.wrapper}`}>
      {brands.map((brand) => {
        return (
          <Card
            key={brand.id}
            className="p-3"
            style={{ cursor: "pointer" }}
            onClick={() => dispatch(setFilteredBrand(brand))}
            border={brand.id === filteredBrand.id ? "danger" : "light"}
          >
            {brand.name}
          </Card>
        );
      })}
      <HiRefresh
        onClick={handleFiltersReset}
        className={styles.reset}
        size="3rem"
      />
    </Row>
  );
}

export default TypeBar;

// TypeBar.defaultProps = {
//   setSelectedBrand: () => {},
//   setSelectedType: () => {},
//   requestShopData: () => {},
// };
// TypeBar.propTypes = {
//   selectedBrand: PropTypes.object.isRequired,
//   setSelectedBrand: PropTypes.func.isRequired,
//   setSelectedType: PropTypes.func.isRequired,
//   requestShopData: PropTypes.func.isRequired,
// };