import React from "react";
import { SiElectron } from "react-icons/si";
import { useSelector, useDispatch } from "react-redux";
import { FaShoppingBasket } from "react-icons/fa";
import { NavLink, useHistory } from "react-router-dom";
import { Navbar, Nav, Button, Container } from "react-bootstrap";

import {
  ABOUT_ROUTE,
  ADMIN_ROUTE,
  BASKET_ROUTE,
  LOGIN_ROUTE,
  SHOP_ROUTE,
} from "../utils/constants";
import {
  selectAuthStatus,
  selectUserData,
  setIsAuth,
  setUser,
} from "../features/user/userSlice";
import { selectBasketData } from "../features/basket/basketSlice";

import styles from "../styles/Navbar.module.css";

function NavigationBar() {
  const history = useHistory();
  const user = useSelector(selectUserData);
  const basketData = useSelector(selectBasketData);
  const isAuth = useSelector(selectAuthStatus);
  const dispatch = useDispatch();

  const logOut = () => {
    dispatch(setIsAuth(false));
    dispatch(setUser({}));
    localStorage.removeItem("token");
  };

  return (
    <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
      <Container>
        <NavLink
          style={{
            color: "white",
            fontSize: 18,
            fontWeight: "bold",
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
          }}
          to={SHOP_ROUTE}
        >
          <SiElectron size="24px" style={{ marginRight: "5px" }} />
          ElecroPowers
        </NavLink>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav style={{ color: "white" }} className="ml-auto">
            <NavLink className={styles.aboutRoute} to={ABOUT_ROUTE}>
              Contact / Support
            </NavLink>
            {isAuth ? (
              <>
                <NavLink className={styles.basketBtn} to={BASKET_ROUTE}>
                  <FaShoppingBasket size="32px" />
                  {basketData?.basket_devices?.length ? (
                    <div className={styles.basketIndicator}>
                      {basketData.basket_devices.length}
                    </div>
                  ) : null}
                </NavLink>
                {user?.role === "ADMIN" && (
                  <Button
                    variant={"outline-light"}
                    className={`mr-2 ${styles.aPanelBtn}`}
                    onClick={() => history.push(ADMIN_ROUTE)}
                  >
                    Админ панель
                  </Button>
                )}
                <Button
                  className={styles.exitBtn}
                  variant={"outline-light"}
                  onClick={() => logOut()}
                >
                  Выйти
                </Button>
              </>
            ) : (
              <>
                <Button
                  variant={"outline-light"}
                  className={styles.authBtn}
                  onClick={() => {
                    history.push(LOGIN_ROUTE);
                  }}
                >
                  Авторизация
                </Button>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavigationBar;
